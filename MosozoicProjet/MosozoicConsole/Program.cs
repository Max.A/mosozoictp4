﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MosozoicConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 11);

            Console.WriteLine(louis.sayHello());
            Console.WriteLine(louis.roar());
            Console.WriteLine(nessie.sayHello());
            Console.WriteLine(nessie.roar());
            Console.WriteLine(nessie.Hug(louis));

            Console.ReadKey();
        }
    }
}
